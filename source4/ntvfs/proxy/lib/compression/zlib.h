/*
   Basic compression wrappers.

   Copyright (C) 2007 Sam Liddicott <sam@liddicott.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMPRESSION_H

#include <sys/types.h>
#include "lib/crypto/crypto.h"

#define declare_checksum(digest) uint8_t digest[16]
#define checksum_block(checksum, data, length) do { \
       struct MD5Context context; \
       MD5Init(&context); \
       MD5Update(&context, data, length); \
       MD5Final(checksum, &context); \
} while(0)

bool uncompress_block(uint8_t* compr, ssize_t *size, ssize_t max_size);
bool uncompress_block_to(uint8_t * uncompr, uint8_t* compr, ssize_t *size, ssize_t max_size);
uint8_t * uncompress_block_talloc(void* mem_ctx, uint8_t* compr, ssize_t *size, ssize_t max_size);
bool compress_block(uint8_t* uncompr, ssize_t *size);
bool compress_block_to(uint8_t* compr, const uint8_t* uncompr, ssize_t *size, ssize_t max_size);
uint8_t * compress_block_talloc(void* mem_ctx, const uint8_t* uncompr, ssize_t *size);

#endif /* COMPRESSION_H */
