################################################
# Start SUBSYSTEM LIBCOMPRESSION
[SUBSYSTEM::LIBCOMPRESSION]
PUBLIC_DEPENDENCIES = LIBTALLOC LIBCRYPTO ZLIB

# End SUBSYSTEM LIBCOMPRESION
################################################

LIBCOMPRESSION_OBJ_FILES = $(addprefix ntvfs/proxy/lib/compression/, zlib.o)

