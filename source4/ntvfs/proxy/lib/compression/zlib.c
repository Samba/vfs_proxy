/*
   Basic compression wrappers.

   Copyright (C) 2007 Sam Liddicott <sam@liddicott.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "ntvfs/ntvfs.h"
#include "ntvfs/proxy/lib/compression/zlib.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "zconf.h"
#include <zlib.h>

/* uncompress data in *compr to *uncompr, writing the dinal size in *size and
   aborting if the final size exceeds max_size which should be the size of *uncompr */
bool uncompress_block_to(uint8_t * uncompr, uint8_t* compr, ssize_t *size, ssize_t max_size)
{
	int err;
	uLongf comprLen=*size;
	uLongf uncomprLen;

	uncomprLen=max_size;
	if (size == 0 || max_size == 0) return false;

	err=uncompress(uncompr, &uncomprLen, compr, comprLen);

	if (err != Z_OK) {
		return false;
	}

	*size=uncomprLen;

	return true;
}

/* talloc in mem_ctx a block of max_size and incompress *compr there.
   return's talloc'd pointer to uncompressed data, or NULL on failure */
uint8_t * uncompress_block_talloc(void* mem_ctx, uint8_t* compr, ssize_t *size, ssize_t max_size)
{
	uLongf uncomprLen;
	uint8_t * uncompr;

	uncomprLen=max_size;
	uncompr=talloc_zero_size(mem_ctx,uncomprLen);

	if (! uncompress_block_to(uncompr, compr, size, max_size)) {
		talloc_free(uncompr);
		return NULL;
	}

	*size=uncomprLen;

	return uncompr;
}

/* uncompress in-place a compressed buffer. The buffer must be large enough
   to hold the uncompressed data */
bool uncompress_block(uint8_t* compr, ssize_t *size, ssize_t max_size)
{
	uint8_t * uncompr;

	uncompr=uncompress_block_talloc(NULL, compr, size, max_size);

	if (! uncompr) {
		return false;
	}

	memcpy(compr,uncompr,*size);
	talloc_free(uncompr);

	return true;
}

bool compress_block_to(uint8_t* compr, const uint8_t* uncompr, ssize_t *size, ssize_t max_size)
{
	int err;
	uLongf uncomprLen=*size;
	uLongf comprLen=max_size;

	err = compress(compr, &comprLen, uncompr, uncomprLen);
	if (err != Z_OK) {
		return false;
	}
	*size=comprLen;
	return true;
}

/* allocate a buffer to compress *uncompr, and return the pointer.
   If the compressed data is larger than the uncompressed data, don't bother */
uint8_t * compress_block_talloc(void* mem_ctx, const uint8_t* uncompr, ssize_t *size)
{
	int err;
	uLongf uncomprLen=*size;
	/* destination buffer must be at least 0.1% larger than sourceLen plus 12 bytes
	   I add 13 to make up for rounding-down errors in the /10 */
	uLongf comprLen=uncomprLen + uncomprLen/10 + 13;
	uint8_t * compr;

	if (*size==0) return false;
	compr=talloc_zero_size(mem_ctx, comprLen);

	err=compress(compr, &comprLen, uncompr, uncomprLen);
	/* if compression was not worthwhile it will be better not to */
	if (err != Z_OK || comprLen > uncomprLen) {
		talloc_free(compr);
		return NULL;
	}
	*size=comprLen;
	return compr;
}

/* compress in-place a block of data, if it is effectice to do so */
bool compress_block(uint8_t* uncompr, ssize_t *size)
{
	uint8_t * compr;
	if (*size==0) return false;

	compr=compress_block_talloc(NULL, uncompr, size);
	if (! compr) return false;

	memcpy(uncompr,compr,*size);
	talloc_free(compr);
	return true;
}
