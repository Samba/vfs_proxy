dnl # Enable caching and compression
SMB_EXT_LIB(ZLIB,-lz)

AC_CHECK_LIB(z, compress, [
	SMB_ENABLE(ZLIB, YES)
], [
	SMB_ENABLE(ZLIB, NO)
])
