/*
   Unix SMB/PROXY simplistic cache engine

   Copyright (C) 2007 Sam Liddicott <sam@liddicott.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CACHE_H

#include <sys/types.h>
#include "ntvfs/ntvfs.h"

#define CACHE_DEBUG_LEVEL 1

enum cache_actions {
#define CACHE_NONE 0/* can't cache, won't cache */
		CACHE_FILL_bit, /* populate the cache but don't read from it */
#define CACHE_FILL (1 << CACHE_FILL_bit)
		CACHE_READ_bit, /* read from the cache as well as write to it */
#define CACHE_READ (1 << CACHE_READ_bit)
		CACHE_READ_AHEAD_bit, /* fill by read-ahead */
#define CACHE_READ_AHEAD (1 << CACHE_READ_AHEAD_bit)
		CACHE_VALIDATE_bit, /* bulk re-validate existing cache */
#define	CACHE_VALIDATE (1 << CACHE_VALIDATE_bit)
};

typedef uint8_t cache_state;

#define CACHE_FILLING(cache) !!(cache && cache->status & CACHE_FILL)

/* cache file entry, pointed to by the backend file struct, e.g. proxy_file
 * Contains information pertinent to the cachability of the file */
struct proxy_file;

struct cache_context {
	char* root;
	char* server;
	char* share;
	char* prefix;
};

struct cache_file_entry {
	struct cache_context* context;
	struct proxy_file* f;
	int fd;
	cache_state status; /* current state of this cache file */
	ssize_t readahead_window; /* how much to read-ahead by */
	ssize_t readahead_extent; /* the extent of read-aheads requested */
	ssize_t validated_extent; /* the extent we can read from in the cache */
	uint16_t smbpid; /* supposed to be some kind of session number, unique per tree-connect at any instant */
	char *cache_name;
	char *pathname;
};

struct cache_context *new_cache_context(TALLOC_CTX * memctx, const char* root, const char *server, const char *share);
void cache_file_stale(struct cache_file_entry *cache);
#define cache_handle_novalidate(handle) do { if (handle->cache) handle->cache->status&=~CACHE_VALIDATE; } while(0)
#define cache_handle_stale(handle) do { if (handle->cache) cache_file_stale(handle->cache); } while(0)
void cache_new_file(struct cache_file_entry * cache, struct proxy_file *f, const char* filename, int readahead, bool oplock);
void cache_file_state(struct cache_file_entry * cache, cache_state state);
#define cache_handle_validated(handle, offset) do \
{ if (handle && handle->cache) { \
  cache_validated(handle->cache, offset); \
} } while (0);
void cache_validated(struct cache_file_entry *cache, off_t offset);
#define cache_handle_save(handle, data, length, offset) do \
{ if (handle && handle->cache) { \
  cache_save(handle->cache, data, length, offset); \
} } while (0);
void cache_save(struct cache_file_entry *cache, const uint8_t* data, ssize_t size, off_t offset);
/* read from cache. If valided is not null then *validated is set to the extent
   of the data from the cache that is valid.
     If *validated is 0 then the read data needs validating
     If it is readx.out.nread then it is all valid.
     If it is somewhere in between, the caller may choose to truncate then
   read to the validated section, or re-validate the whole lot */
NTSTATUS cache_smb_raw_read(struct cache_file_entry *cache,
				struct ntvfs_module_context *ntvfs,
				struct ntvfs_request *req,
				union smb_read *io,
				ssize_t* validated);
/* read from the cache, returning the checksum of what was read */
NTSTATUS cache_smb_raw_checksum(struct cache_file_entry *cache,
				offset_t offset, ssize_t* length, uint8_t digest[16]);
ssize_t cache_raw_read(struct cache_file_entry *cache, uint8_t* data, off_t offset, ssize_t size);
#define cache_handle_len(handle) ((handle && handle->cache)?(cache_len(handle->cache)):(-1))
ssize_t cache_len(struct cache_file_entry *cache);
struct cache_file_entry * cache_filename_open(struct cache_context *cache_context,
									 struct proxy_file *f,
									 const char* filename,
									 bool oplock,
									 int readahead_window);
struct cache_file_entry * cache_fileid_open(struct cache_context *cache_context,
									 struct proxy_file *f,
									 const uint64_t* id,
									 bool oplock,
									 int readahead_window);
void cache_close(struct cache_file_entry *cache);
void cache_beopen(struct cache_file_entry *cache);
void cache_reopen(struct cache_file_entry *cache);
void cache_md5sum(uint8_t digest[16], uint8_t *data, ssize_t size);

#endif /* CACHE_H */
