/*
   Unix SMB/PROXY cache engine.

   Copyright (C) 2008 Jelmer Vernooij <jelmer@samba.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "../lib/torture/torture.h"
#include "ntvfs/proxy/lib/cache/cache.h"


static bool test_create(struct torture_context *tctx)
{
	struct cache_context *cache = new_cache_context(tctx, "root", "share", "user");

	torture_assert(tctx, cache != NULL, "cache context");

	return true;
}

struct torture_suite *torture_cache(void)
{
	struct torture_suite *suite = torture_suite_create(NULL, "CACHE");

	torture_suite_add_simple_test(suite, "create", test_create);

	return suite;
}
