/*
   Unix SMB/PROXY cache engine.

   Copyright (C) 2007 Sam Liddicott <sam@liddicott.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "libcli/raw/libcliraw.h"
#include "libcli/smb_composite/smb_composite.h"
#include "auth/auth.h"
#include "auth/credentials/credentials.h"
#include "ntvfs/ntvfs.h"
#include "lib/util/dlinklist.h"
#include "param/param.h"
#include "cache.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "zconf.h"
#include "ntvfs/proxy/lib/compression/zlib.h"

/* like mkdir -p */
int mkpath(const char* at, const char* pathname, mode_t mode)
{
	char path[MAXPATHLEN];
	char* next;
	int len;
	struct stat stat_path;

	strncpy(path, at, MAXPATHLEN);
	len=strlen(path);
	next=path+len;
	if (len==0 || (path[len-1]!='/' && len < MAXPATHLEN-1)) {
		strncat(path,"/",MAXPATHLEN - len - 1);
		len=strlen(path);
		next=path+len;
	}
	strncat(path, pathname, MAXPATHLEN - len - 1);

	/* get to the non / portions of pathname */
	while (*next=='/') next++;
	next=strchr(next, '/');

	while (! next || *next) {
		if (next) *next=0;
		if (stat(path, &stat_path)) {
			/* doesn't exist, try to create it */
			if (errno!=ENOENT) return -1;
			if (mkdir(path, mode)) return -1;
		} else if (! S_ISDIR(stat_path.st_mode)) {
			/* exists, but not a directory */
			errno=ENOTDIR;
			return -1;
		}
		/* so it's a directory (now) */
		if (! next) return 0;
		*next='/';
		while (*next=='/') next++;
		next=strchr(next+1, '/');
	}
	return 0;
}

struct cache_context *new_cache_context(TALLOC_CTX * memctx, const char* root, const char *server, const char *share)
{
	struct cache_context *context=talloc_zero(memctx, struct cache_context);
	if (! context) return NULL;

	context->root=talloc_strdup(context, root);
	context->server=strlower_talloc(context, server);
	/* share names are never case sensitive */
	context->share=strlower_talloc(context, share);
	context->prefix=talloc_asprintf(context,"%s/%s/", context->server, context->share);
	DEBUG(5,("New cache context: root=%s, prefix=%s\n",
			 context->root, context->prefix));
	return context;
}

void clear_cache_storage(struct cache_file_entry *cache)
{
//	if (cache->store_name && *(cache->store_name)) {
//		unlink(cache->store_name);
//		talloc_free(cache->store_name);
//		cache->store_name=NULL;
//	}
}

/* op-lock has probably been broken. We can no-longer read-cache for this file
   in fact the whole cache is invalid for this file now, but we can use it as
   a reference for delta-compression syncs if the remote server supports it */
void cache_file_stale(struct cache_file_entry *cache)
{
	if (cache) {
		cache->status &= ~ CACHE_READ;
		cache->validated_extent=0;
		DEBUG(CACHE_DEBUG_LEVEL,("Cache file %s no longer good to read from\n",cache->cache_name));
	}
}

void cache_close(struct cache_file_entry *cache) {
	if (cache && cache->fd>=0) {
		close(cache->fd);
		cache->fd=-1;
	}
}

void cache_beopen(struct cache_file_entry *cache) {
	if (cache->fd<=0) cache_reopen(cache);
}

void cache_reopen(struct cache_file_entry *cache) {
	cache_close(cache);
	cache->fd=open(cache->pathname,O_RDWR | O_CREAT,S_IRWXU | S_IROTH | S_IXOTH);
}

void cache_create(struct cache_file_entry *cache, int readahead_window)
{
	if (cache->status != CACHE_NONE && cache->fd<0) {
		mkpath(cache->context->root, cache->context->prefix, S_IRWXU | S_IROTH | S_IXOTH);
		cache->pathname=talloc_asprintf(cache, "%s/%s",
											  cache->context->root,
											  cache->cache_name);
		cache_reopen(cache);
	}
	cache->readahead_window=readahead_window;
}

void cache_new_file(struct cache_file_entry * cache, struct proxy_file *f,
					const char* filename,
					int readahead_window, bool oplock)
{
	cache->f=f;
	cache->fd=-1;
	cache->status=CACHE_FILL | CACHE_READ_AHEAD;
	DEBUG(5,("NEW CACHE FILE %x oplock %x\n",cache->status,oplock));
	if (oplock) cache->status|=CACHE_READ | CACHE_VALIDATE;
	cache_create(cache, readahead_window);
}

void cache_file_state(struct cache_file_entry * cache, cache_state state)
{
	cache->status=state;
}

static ssize_t flen(int fd)
{
	struct stat stats;

	if (fstat(fd, &stats)==0) {
		return (ssize_t)stats.st_size;
	}
	return -1;
}

ssize_t cache_len(struct cache_file_entry *cache)
{
	return flen(cache->fd);
}

void cache_validated(struct cache_file_entry *cache, off_t offset)
{
	if (cache->validated_extent <= offset) {
		cache->validated_extent=offset;
	}
	if (cache->readahead_extent < offset) {
		cache->readahead_extent=offset;
	}
}

void cache_save(struct cache_file_entry *cache, const uint8_t* data, ssize_t size, off_t offset)
{
	if (cache && size > 0 && cache->status & CACHE_FILL) {
		off_t len;
		ssize_t written;
		if (cache->fd < 0) return;

		DEBUG(1,("Consider saving in cache\n"));
		/* until we manage sparse caching, the offset must be within, or adjacent
		 * to what has been cached, without an intervening gap - as we have no way
		 * of encoding that the gap is not valid cache */
		len=flen(cache->fd);
		if (offset <= len+1 && lseek(cache->fd, offset, SEEK_SET)>=0) {
			/* save cache value */
			written=write(cache->fd, data, size);
			/* only extend the validated extent if the cache is good for reading from */
			if (cache->status & CACHE_READ && cache->validated_extent < (offset + size)
			    /* but don't extend if it would leave unvalidated gaps */
			    && cache->validated_extent >= offset)
				cache->validated_extent=offset + size;
			DEBUG(CACHE_DEBUG_LEVEL,("Save cache written at %d %d bytes\n",(int)offset,(int)written));

			if (cache->readahead_extent < offset+size) {
				cache->readahead_extent=offset+size;
			}
		} else DEBUG(CACHE_DEBUG_LEVEL,("No sparse cache support, ignoring write at %d which is beyond %d\n",(int)offset,(int)len));
	} else DEBUG(CACHE_DEBUG_LEVEL,("No caching on file\n"));
}

ssize_t cache_raw_read(struct cache_file_entry *cache, uint8_t* data, off_t offset, ssize_t size)
{
	return pread(cache->fd, data, size, offset);
}

/* This function should track changes in rawreadwrite.c/smb_raw_read_send */
NTSTATUS cache_smb_raw_read(struct cache_file_entry *cache,
				struct ntvfs_module_context *ntvfs,
				struct ntvfs_request *req,
				union smb_read *rd,
				ssize_t* validated)
{
	/* Based on pvfs_read.c/pvfs-read */
	ssize_t ret;
	uint32_t maxcnt;
	uint32_t mask;
	off_t len;

	if (validated) *validated=0;

	DEBUG(CACHE_DEBUG_LEVEL,("Actually trying to read from the cache len %d offset %d\n",(int)rd->generic.in.maxcnt,(int)rd->generic.in.offset));
	/* Can we actually read ahead on this file anyway? */
	if (! cache) {
		DEBUG(CACHE_DEBUG_LEVEL,("Not a valid cache\n"));
		return NT_STATUS_UNSUCCESSFUL;
	}

	if (cache->fd < 0) {
		return NT_STATUS_INVALID_HANDLE;
	}

	if (! (cache->status & CACHE_READ) ) {
		DEBUG(CACHE_DEBUG_LEVEL,("Cache %s not in read mode: %x\n",cache->cache_name, cache->status));
		return NT_STATUS_UNSUCCESSFUL;
	}

	/* mapping should have been done by caller */
	if (rd->generic.level != RAW_READ_READX) {
		return NT_STATUS_INVALID_LEVEL;
	}

	mask = SEC_FILE_READ_DATA;
	if (rd->generic.in.read_for_execute) {
		mask |= SEC_FILE_EXECUTE;
	}
#warning need to check access mask
/*	if (!(f->access_mask & mask)) {
		return NT_STATUS_ACCESS_DENIED;
	}
*/
	maxcnt = rd->generic.in.maxcnt;
	if (maxcnt > UINT16_MAX && req->ctx->protocol < PROTOCOL_SMB2) {
		maxcnt = 0;
	}

	len=flen(cache->fd);
	/* if read would return 0 bytes then we must fail unless we know the cache is fully populated */
	if (/*! fully-populated && */rd->generic.in.offset >= len) {
		DEBUG(CACHE_DEBUG_LEVEL,("Can't read %d bytes from position %d which is %d beyond cache length %d\n",(int)maxcnt,(int)rd->generic.in.offset,(int)(maxcnt + rd->generic.in.offset - len),(int)len));
		return NT_STATUS_UNSUCCESSFUL;
	}

#warning need to check locks
	/* er... I guess we need to be spying on locks, or something */
	/*
	status = pvfs_check_lock(pvfs, f, req->smbpid,
				 rd->generic.in.offset,
				 maxcnt,
				 READ_LOCK);
	if (!NT_STATUS_IS_OK(status)) {
		return status;
	}
	*/

	ret = pread(cache->fd,
		    rd->generic.out.data,
		    maxcnt,
		    rd->generic.in.offset);

	if (ret == -1) {
		int e=errno;
		/* Why do I assume it is running on a unix? */
		DEBUG(CACHE_DEBUG_LEVEL,("Error %x Read %d bytes from cache offset %d\n",e,ret,(int)rd->generic.in.offset));
		return map_nt_error_from_unix(e);
	}

	/* Does the cache need to do handle position tracking? */
	/* f->handle->position = f->handle->seek_offset = rd->generic.in.offset + ret; */

	rd->generic.out.nread = ret;
	rd->generic.out.remaining = 0xFFFF;
	rd->generic.out.compaction_mode = 0;

	/* How much of what we have is validated cache? */
	if (! validated); /* just yawn */
	else if (cache->validated_extent < rd->generic.in.offset)
		*validated=0;
	else if (cache->validated_extent >= rd->generic.in.offset +
			rd->generic.out.nread)
		*validated=rd->generic.out.nread;
	else *validated=cache->validated_extent - rd->generic.in.offset;

	DEBUG(CACHE_DEBUG_LEVEL,("Cache successfully read %d bytes validated %d\n",(int)ret, (int)((validated)?(*validated):-1) ));

	return NT_STATUS_OK;
}

/* Take MD5 checksum of part of cache file */
NTSTATUS cache_smb_raw_checksum(struct cache_file_entry *cache,
				offset_t offset, ssize_t* length, uint8_t digest[16])
{
	struct MD5Context context;
	int ret=0;
	ssize_t completed=0;
	ssize_t remaining=*length;
#define buffer_size 4096
	uint8_t buffer[buffer_size];

	MD5Init(&context);

	while(remaining>0) {
		ret = pread(cache->fd, buffer, MIN(buffer_size, remaining), offset);
		if (ret<=0) break;
		completed+=ret;
		offset+=ret;
		remaining-=ret;
		MD5Update(&context, buffer, ret);
	}

	MD5Final(digest, &context);
	*length=completed;

	if (length==0) return NT_STATUS_UNSUCCESSFUL;
	return NT_STATUS_OK;
}

/* Set up caching. We want to pass a reference to the file we opened as
 * part of the cache key. We see this note in interfaces.h:
		NOTE: fname can also be a pointer to a
		 uint64_t file_id if create_options has the
		 NTCREATEX_OPTIONS_OPEN_BY_FILE_ID flag set
 * which is nice! We need to notice! */

struct cache_file_entry * cache_filename_open(struct cache_context *cache_context,
									 struct proxy_file *f,
									 const char* filename,
									 bool oplock,
									 int readahead_window)
{
	struct cache_file_entry *cache=talloc_zero(f, struct cache_file_entry);

	if (cache) {
		cache->context=talloc_reference(cache, cache_context);
		cache->cache_name=talloc_asprintf(cache,"%s/cache-%d-f-%s",
										  cache->context->prefix,0,filename);
		DEBUG(1,("Session %d open proxied for file: %s, cache %s RA %d\n",
				 0, filename, cache->cache_name, readahead_window));

		cache_new_file(cache, f, cache->cache_name, readahead_window, oplock);
	}
	return cache;
}

struct cache_file_entry * cache_fileid_open(struct cache_context *cache_context,
									 struct proxy_file *f,
									 const uint64_t* id,
									 bool oplock,
									 int readahead_window)
{

	struct cache_file_entry *cache=talloc_zero(f, struct cache_file_entry);

	if (cache) {
		cache->context=talloc_reference(cache, cache_context);
		cache->cache_name=talloc_asprintf(cache,"%s/cache-%d-id-%lld",
										  cache->context->prefix,0,*id);
		DEBUG(1,("Session %d open proxied for file-id: %lld, cache %s RA %d\n",
				 0, *id,cache->cache_name, readahead_window));

		cache_new_file(cache, f, cache->cache_name, readahead_window, oplock);
	}
	return cache;
}
