################################################
# Start SUBSYSTEM SMB_PROXY_CACHE
[SUBSYSTEM::SMB_PROXY_CACHE]
PUBLIC_DEPENDENCIES = LIBTALLOC

# End SUBSYSTEM SMB_PROXY_CACHE
################################################

SMB_PROXY_CACHE_OBJ_FILES = ntvfs/proxy/lib/cache/cache.o

[SUBSYSTEM::torture_SMB_PROXY_CACHE]
PRIVATE_DEPENDENCIES = SMB_PROXY_CACHE

torture_SMB_PROXY_CACHE_OBJ_FILES = $(addprefix ntvfs/proxy/lib/cache/tests/, cache.o)

$(eval $(call proto_header_template,ntvfs/proxy/lib/cache/tests/proto.h,$(torture_SMB_PROXY_CACHE_OBJ_FILES:.o=.c)))

