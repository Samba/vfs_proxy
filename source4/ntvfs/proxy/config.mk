################################################
# Start MODULE ntvfs_proxy
[MODULE::ntvfs_proxy]
INIT_FUNCTION = ntvfs_proxy_init
SUBSYSTEM = ntvfs
PRIVATE_DEPENDENCIES = \
                LIBCLI_SMB LIBCLI_RAW LIBCOMPRESSION SMB_PROXY_CACHE RPC_NDR_PROXY
# End MODULE ntvfs_proxy
################################################

ntvfs_proxy_OBJ_FILES = $(ntvfssrcdir)/proxy/vfs_proxy.o

mkinclude lib/compression/config.mk
mkinclude lib/cache/config.mk

