/*
   Unix SMB/CIFS implementation.
   Samba utility functions
   Copyright (C) Jelmer Vernooij <jelmer@samba.org> 2008

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "torture/torture.h"

static bool test_simple(struct torture_context *tctx, struct smbcli_state *cli)
{
	/* FIXME */



	return true;
}

struct torture_suite *torture_proxy(TALLOC_CTX *parent)
{
	struct torture_suite *suite = torture_suite_create(parent, "PROXY");

	torture_suite_add_1smb_test(suite, "simple", test_simple);

	return suite;
}

