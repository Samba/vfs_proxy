#! /bin/bash
#  make-srpm: srpm builder, helps build srpm out of rcs sources
# svn helper; export correct git release and patches ready to
# build src.rpm

#  Copyright (C) 2008 Sam Liddicott <sam@liddicott.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


svn_toplevel() {
	test -d .svn -a ! -d ../.svn
}

svn_export() {
	mkdir -p "`dirname "$1"`"
	svn export -q ${ORIGIN:+-r $ORIGIN} . "$1"
}

svn_archive() {
	rm -fr "$SRC_EXPORT/$1" &&
	svn_export "$SRC_EXPORT/$1" &&
	( cd "$SRC_EXPORT" && tar -cvf - "$1"; rm -fr "$1" ) > "$2"
}

prepare_svn_src() {
  NAME="${1:-$Name}"
  VERSION="${2:-${VERSION:-$Version}}"
  : ${TMPDIR:=$SRC_EXPORT}

  case "$RELEASE" in
    THIS|LOCAL|"") _RELEASE=HEAD; LOCAL=1;;
    ?*) _RELEASE="$RELEASE"; LOCAL="";;
  esac
  RELEASE_COMMIT="`sed '/Revision: /!d;s/^[^:]*: *//' <( svn info -r $_RELEASE )`"
  VERSION_INFO="$RELEASE_COMMIT"

  THIS_COMMIT="`sed '/Revision: /!d;s/^[^:]*: *//' <( svn info )`"

  : ${ORIGIN:="$_RELEASE"}
  ORIGIN_COMMIT="`sed '/Revision: /!d;s/^[^:]*: *//' <( svn info ${ORIGIN:+ -r $ORIGIN} )`"
  test -z "$ORIGIN" && die "Can't find origin $ORIGIN to use as pristine source for $_RELEASE"

  RELEASE_SUFFIX="SVN_$RELEASE_COMMIT"

  # append date if we are including uncomitted files
  if test -n "$LOCAL"
  then
    RELEASE_SUFFIX="`date +%Y%m%d`_${RELEASE_SUFFIX:+${RELEASE_SUFFIX}_}local"
  fi

  test $ORIGIN_COMMIT -gt $THIS_COMMIT && die "Origin $ORIGIN is later then checkout!"

  # remove a "release-" prefix from the name
  RELEASE_NAME="$NAME-${RELEASE_TAG:+$RELEASE_TAG-}${RELEASE_COMMIT}${RELEASE_SUFFIX:+_$RELEASE_SUFFIX}"

  # if GIT_ORIGIN is like origin/blah then we can't use it as a filename part
  ORIGIN_NAME="$NAME-${ORIGIN_COMMIT}"

  TAR_PREFIX="$ORIGIN_NAME"
  TAR_NAME="$TAR_PREFIX.tar.gz"

  SPEC_VERSION="$VERSION"

  # make a tar in SRC_EXPORT, where the patches will also go
  # (should use git version from origin)
  SVN_INFO="$RPM_GIT_INFO" \

  svn_archive "$ORIGIN_NAME" "$TMPDIR/$TAR_NAME"

  # make patches
  > "$TMPDIR/patches.list"

  if test "$ORIGIN_COMMIT" != "$RELEASE_COMMIT"
  then
    # one big git-diff
    PATCH_NAME="$ORIGIN_NAME-$RELEASE_NAME.patch"
    svn diff "-r $ORIGIN_COMMIT":"$RELEASE_COMMIT" > "$TMPDIR/$PATCH_NAME"
    test -s "$TMPDIR/$PATCH_NAME" && echo "Patch1: $PATCH_NAME" >> "$TMPDIR/patches.list"
  fi

  # also uncomitted patches?
  if test -n "$LOCAL"
  then
    set `wc -l "$TMPDIR/patches.list"`
    PATCH_NO="$(( $1 + ${START_PATCH_NO:-0} ))"

    # any local files in the current change set
#    svn diff -r BASE:BASE > "local-cached.patch"
#    if test -s local-cached.patch
#    then PATCH_NO="`expr "${PATCH_NO:-0}" \+ 1`"
#         echo "Patch$PATCH_NO: local-cached.patch" >> patches.list
#    else rm local-cached.patch
#    fi

    # any local files NOT in the current change set
    pwd
    (cd "$TOPDIR" && svn diff) > "$TMPDIR/local.patch"
    if test -s "$TMPDIR/local.patch"
    then PATCH_NO="`expr "${PATCH_NO:-0}" \+ 1`"
         echo "Patch$PATCH_NO: local.patch" >> "$TMPDIR/patches.list"
    else rm "$TMPDIR/local.patch"
    fi
  fi
  # fixup patches into the spec file
  sed -e "s/^Patch\([0-9]*\):.*/%patch\1 -p$PATCH_LEVEL/" < "$TMPDIR/patches.list" > "$TMPDIR/patches.apply"

  SPEC_RELEASE="$RELEASE_SUFFIX"
}
