#makesrpm-RELEASE: HEAD
#makesrpm-ORIGIN_PATTERN: release-4*alpha*
#makesrpm-SOURCE_PATHS: lib/tdb lib/replace

%define dirname %makesrpm_tarprefix

Name: libtdb
Source: %makesrpm_tarname
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
URL: http://tdb.samba.org/
License: LGPL3 or later
Summary: Samba tdb library
Group: Development/Libraries/C and C++
PreReq: /sbin/ldconfig
Version: 1.1.1
Release: 0.%{main_release}.alpha%{alpha_version}%{?dist}
Epoch: 0

%description
This package includes the talloc library.

%package devel
License: LGPL v3 or later
Summary: Samba tdb library header files
Group: Development/Libraries/C and C++
Requires: libtdb  = %{version}

%description devel
This package contains the static libraries and header files needed to
develop programs which make use of the tdb programming interface.

%prep
%setup -q -n %{dirname}

%build

cd lib/tdb
sh ./autogen.sh
CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -DLDAP_DEPRECATED" GIT_INFO=%{rpm_git_info} %configure \
	--with-fhs --enable-fhs --prefix=/usr
make -j5

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/

cd lib/tdb
make install DESTDIR="$RPM_BUILD_ROOT"

#This makes the right links, as rpmlint requires that the
#ldconfig-created links be recorded in the RPM.
ln -s libtdb.so.1 $RPM_BUILD_ROOT/%{_libdir}/libtdb.so
/sbin/ldconfig -N -n $RPM_BUILD_ROOT/%{_libdir}

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/libtdb.so.*
%{_bindir}/tdbbackup
%{_bindir}/tdbdump
%{_bindir}/tdbtool
#%{_bindir}/tdbtorture

%files devel
%defattr(-,root,root)
%{_includedir}/tdb.h
%{_libdir}/libtdb.a
%{_libdir}/libtdb.so
%{_libdir}/pkgconfig/tdb.pc

%changelog
* Fri Nov 7 2008 Sam Liddicott <sam@liddicott.com>
- Update for simpler make-srpm builder
* Mon Aug 18 2008 Sam Liddicott <sam@liddicott.com>
- First go, based on some of AB's stuff
