#makesrpm-RELEASE: HEAD
#makesrpm-ORIGIN_PATTERN: release-4*alpha*
#makesrpm-SOURCE_PATHS: lib/talloc lib/replace

%define dirname %makesrpm_tarprefix

Name: libtalloc
Source: %makesrpm_tarname
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
URL: http://talloc.samba.org/
License: LGPL3 or later
Summary: Samba talloc library
Group: Development/Libraries/C and C++
PreReq: /sbin/ldconfig
Version: 1.2.0
Release: 0.%{main_release}.alpha%{alpha_version}%{?dist}
Epoch: 0

%description
This package includes the talloc library.

%package devel
License: GPL v3 or later
Summary: Libraries and Header Files to Develop Programs with talloc Support
Group: Development/Libraries/C and C++
Requires: libtalloc  = %{version}

%description devel
This package contains the static libraries and header files needed to
develop programs which make use of the talloc programming interface.

%prep
%setup -q -n %{dirname}

%build

cd lib/talloc
sh ./autogen.sh
CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -DLDAP_DEPRECATED" GIT_INFO=%{rpm_git_info} %configure \
	--with-fhs --enable-fhs
make -j5

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/

cd lib/talloc
make install DESTDIR="$RPM_BUILD_ROOT"

#This makes the right links, as rpmlint requires that the
#ldconfig-created links be recorded in the RPM.
/sbin/ldconfig -N -n $RPM_BUILD_ROOT/%{_libdir}
ln -s libtalloc.so.1 $RPM_BUILD_ROOT/%{_libdir}/libtalloc.so

rm -f $RPM_BUILD_ROOT/usr/share/swig/1.3.29/talloc.i $RPM_BUILD_ROOT/usr/usr/share/swig/1.3.29/talloc.i

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/libtalloc.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/talloc.h
%{_libdir}/libtalloc.a
%{_libdir}/libtalloc.so
%{_libdir}/pkgconfig/talloc.pc
/usr/share/man/man3/talloc.3.gz
#/usr/share/swig/1.3.29/talloc.i

%changelog
* Fri Nov 7 2008 Sam Liddicott <sam@liddicott.com>
- Update for simpler make-srpm builder
* Mon Aug 18 2008 Sam Liddicott <sam@liddicott.com>
- First go, based on some of AB's stuff
